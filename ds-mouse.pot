# ds-mouse (version ...)
# Copyright (C) 2017 Dave (david@daveserver.info)
# This file is distributed under the same license as the ds-mouse package. (GPL v.3)
# Translation set prepared by Robin (antiX community), 2021.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ds-mouse version (...)\n"
"Report-Msgid-Bugs-To: antixforum.com\n"
"POT-Creation-Date: 2021-09-16 16:23+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: https://www.transifex.com/anticapitalista/antix-development/dashboard/\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ds-mouse:21
msgid ""
"out-of-range value for acceleration or threshold, so applying xset default "
"values"
msgstr ""

#: ds-mouse:51
msgid "Help:"
msgstr ""

#: ds-mouse:52
msgid "-a   | set mouse motion (acceleration and threshold)"
msgstr ""

#: ds-mouse:53
msgid "-b   | set button order for left and right hand"
msgstr ""

#: ds-mouse:54
msgid "-s   | set cursor size"
msgstr ""

#: ds-mouse:55
msgid "-all | set mouse motion, button order, and cursor size"
msgstr ""

#: ds-mouse:56
msgid "-h   | show this help dialog"
msgstr ""

#: ds-mouse:57
msgid "No option start settings gui"
msgstr ""
