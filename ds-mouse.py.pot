# ds-mouse.py (version ...)
# Copyright (C) 2017 Dave (david@daveserver.info)
# This file is distributed under the same license as the ds-mouse package. (GPL v.3)
# Translation set created by Anticapitalista, 2021. Modified by Robin 2021
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ds-mouse.py (version ...)\n"
"Report-Msgid-Bugs-To: antixforum.com\n"
"POT-Creation-Date: 2021-09-16 16:22+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: https://www.transifex.com/anticapitalista/antix-development/dashboard/\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Header of a message box
#: ds-mouse.py:19
msgid "Error"
msgstr ""

#. Header of a message box
#: ds-mouse.py:26
msgid "Success"
msgstr ""

#. Error Message
#: ds-mouse.py:99
msgid "Could not run ds-mouse -all"
msgstr ""

#. Default response after »Apply« button was pressed.
#: ds-mouse.py:101
msgid "All Options Set"
msgstr ""

#. Error message
#: ds-mouse.py:110
msgid "Could not run ds-mouse -a"
msgstr ""

#. Default response after »Reset« button for mouse accelleration settings was pressed.
#: ds-mouse.py:112
msgid "Mouse Acceleration Reset"
msgstr ""

#. Error message
#: ds-mouse.py:124
msgid "Could not run ds-mouse -s"
msgstr ""

#. Default response after »Reset« button for mouse coursor size setting was pressed.
#: ds-mouse.py:126
msgid "Cursor Size Reset"
msgstr ""

#. Error message
#: ds-mouse.py:180
msgid ""
"Could not enable. \n"
" Please edit ~/.desktop-session/startup manually"
msgstr ""

#. Default response after On/Off Switch for enabling/disabling mouse configuration for system boot was set.
#: ds-mouse.py:184
msgid "Mouse configuration will load on startup"
msgstr ""

#. Error message
#: ds-mouse.py:189
msgid ""
"Could not disable. \n"
" Please edit ~/.desktop-session/startup manually"
msgstr ""

#. Default response after On/Off switch for enabling/disenabling mouse configuration for system boot was set.
#: ds-mouse.py:193
msgid "Mouse configuration will not load on startup"
msgstr ""

#. Header in Window frame
#: ds-mouse.py:199
msgid "Mouse Options"
msgstr ""

#. Header for acceleration settings field
#: ds-mouse.py:205
msgid "Mouse Acceleration"
msgstr ""

#. Descriptor of acceleration setting
#: ds-mouse.py:206
msgid "Acceleration (Multiplier)"
msgstr ""

#. Descriptor of acceleration setting
#: ds-mouse.py:216
msgid "Threshold (Pixels)"
msgstr ""

#. Header for button order settings field
#: ds-mouse.py:231
msgid "Button Order"
msgstr ""

#. Option in dropdown menu of button order field
#: ds-mouse.py:234
msgid "Right hand layout"
msgstr ""

#. Option in dropdown menu of button order field
#: ds-mouse.py:235
msgid "Left hand layout"
msgstr ""

#. Header for cursor size settings field
#: ds-mouse.py:243
msgid "Cursor Size"
msgstr ""

#. Descriptor of cursor size setting
#: ds-mouse.py:244
msgid "Size (in pixels)"
msgstr ""

#. Descriptor of cursor design/»theme« setting
#: ds-mouse.py:259
msgid "Cursor Theme"
msgstr ""

#. Information concerning change of cursor design/»theme«
#: ds-mouse.py:260
msgid ""
"May require logout/login \n"
"to see the changes."
msgstr ""

#. Button label
#: ds-mouse.py:262
msgid "Change cursor theme"
msgstr ""

#. Header for startup activation/deactivation settings field 
#: ds-mouse.py:267
msgid "Startup"
msgstr ""

#. Descriptor of On/Off switch for enabling/disabling mouse configuration for system boot
#: ds-mouse.py:268
msgid "Enable or Disable mouse configuration on startup\n"
msgstr ""
