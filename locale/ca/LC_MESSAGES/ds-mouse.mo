��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  @   �  Q   �  <   M  $   �  "   �     �  *   �  o              	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2021
Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 -a   | defineix el moviment del ratolí (acceleració i llindar) -all | defineix el moviment del ratolí, l'ordre dels botons i la mida del cursor -b   | defineix l'ordre dels botons per destres i esquerrans -h   | mostra aquest diàleg d'ajuda -s   | defineix la mida del cursor Ajuda: Sense opcions, executa els ajustos amb IGU El valor per l'acceleració o el llindar està fora de marges, o sigui que s'aplicaran els valors per omissió. 