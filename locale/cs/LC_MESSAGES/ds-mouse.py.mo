��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  0   �  C   (  ;   l  '   �      �     �  '   �  \   %           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: el Presidento, 2021
Language-Team: Czech (https://www.transifex.com/anticapitalista/teams/10162/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 -a   | nastaví pohyb myši (zrychlení a práh) -all | nastaví pohyb myši, pořadí tlačítek a velikost kurzoru -b   | nastaví pořadí tlačítek pro levou a pravou ruku -h   | zobrazí tento dialog nápovědy -s   | nastaví velikost kurzoru Nápověda: Žádná volba pro start nastavení gui hodnota pro zrychlení nebo práh je mimo rozsah, takže se použijí výchozí hodnoty xset 