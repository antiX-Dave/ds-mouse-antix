��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  y  &  [   �  V   �  V   S  %   �  $   �     �  j     �   �           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Robin, 2021
Language-Team: German (https://www.transifex.com/anticapitalista/teams/10162/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
   -a   | Mauszeigerreaktion definieren
         (Faktor und Schwellwert der Beschleunigung)   -all | Einrichten von Mausbeschleunigung,
         Tastenzuweisung und Zeigergröße   -b   | Tastenzuweisung für Rechtshänder- oder
         Linkshändermaus einstellen   -h   | Diese Programmhilfe anzeigen   -s   | Mauszeigergröße festlegen Hilfe:
  Befehlszeilenoptionen:   Wenn keine der Befehlszeilenoptionen angegeben ist,
  wird die graphische Benutzeroberfläche gestartet. Der Wert für Beschleunigung oder Aktivierungsschwelle liegt außerhalb der zulässigen Spanne. Daher wird er durch »xset« Standardwerte ersetzt. 