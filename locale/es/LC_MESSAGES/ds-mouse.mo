��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  ?   �  Z   �  H   T  #   �  %   �     �  G   �  p   6           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Senpai <senpai99@hotmail.com>, 2021
Language-Team: Spanish (https://www.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
 -a | establecer el movimiento del mouse (aceleración y umbral) -all | establecer el movimiento del mouse, el orden de los botones y el tamaño del cursor -b | establecer el orden de los botones para la mano izquierda y derecha -h | mostrar este diálogo de ayuda -s | establecer el tamaño del cursor Ayuda: Sin opción de configuración de inicio de interfaz gráfica de usuario valor fuera de rango para la aceleración o el umbral, por lo que se aplican los valores predeterminados de xset 