��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  A   �  Z   	  L   d  %   �  &   �     �  P     j   V           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Senpai <senpai99@hotmail.com>, 2021
Language-Team: Spanish (Spain) (https://www.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 -a   | establece el movimiento del ratón (aceleración y umbral) -all | establece el movimiento del ratón, el orden de los botones y el tamaño del cursor -b   | establece el orden de los botones para la mano izquierda y la derecha -h   | muestra este diálogo de ayuda -s   | establece el tamaño del cursor Ayuda: Sin opción de inicio para la configuración de la interfaz gráfica de usuario  valor fuera de rango para la aceleración o el umbral, así que se aplican los valores por defecto de xset 