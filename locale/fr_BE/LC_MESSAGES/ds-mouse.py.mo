��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  C   �  V   �  I   R  !   �  $   �     �  D   �  g   /           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Wallon Wallon, 2021
Language-Team: French (Belgium) (https://www.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=2; plural=(n > 1);
 -a   | définir le mouvement de la souris (accélération et seuil) -all | définir le mouvement de la souris, l'ordre des boutons et la taille du curseur -b   | définir l'ordre des boutons pour la main gauche et la main droite -h   | afficher ce message d'aide -s   | définir la taille du curseur Aide : Aucune option de démarrage des paramètres de l'interface graphique valeur hors limites pour l'accélération ou le seuil, donc application des valeurs par défaut de xset 