��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  ;   �  P   �  =   P  &   �  %   �     �  /   �  c              	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2021
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
 -a   | imposta movimento del mouse (accelerazione e soglia) -all | imposta movimento del mouse, ordine dei pulsanti e dimensione del cursore -b   | imposta ordine dei pulsanti per mano destra e sinistra -h   | mostra questa finestra di aiuto -s   | imposta dimensione del cursore Aiuto: Nessuna opzione di avvio delle impostazioni gui valore fuori intervallo per accelerazione o soglia, si applicano quindi i valori di default di xset 