��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  3   �  G   �  9   B     |  #   �     �      �  k   �           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2021
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 -a   | velg musebevegelse (akselerasjon og terskel) -all | velg musebevegelse, knapperekkefølge og musepekerens størrelse -b   | velg knapperekkefølge for venstre og høyre hånd -h   | vis dette hjelpevinduet -s   | velg musepekerens størrelse Hjelp: Intet valgt, åpne oppsettsvindu bruker forvalgte xset-verdier fordi angitt verdi for akselerasjon eller terskel er utenfor gyldig intervall 