��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  :   �  =   �  7   '     _     {     �  !   �  [   �           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Albertus Perkamentus, 2021
Language-Team: Dutch (https://www.transifex.com/anticapitalista/teams/10162/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
 -a | muisbeweging instellen (acceleratie en drempelwaarde) -alle | muisbeweging, knopvolgorde en cursorgrootte instellen -b | knopvolgorde instellen voor linker- en rechterhand -h | toon deze hulp-dialoog -s | cursorgrootte instellen Hulp: Geen optie start instellingen gui Buiten bereik waarde voor acceleratie of drempelwaarde, dus xset standaardwaarden toepassen 