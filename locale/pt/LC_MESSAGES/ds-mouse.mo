��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  .   �  O   �  9   &  "   `  #   �     �  *   �  c   �           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Paulo C., 2021
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
 -a | configurar aceleração e limites do rato -all | configurar movimentação, ordem dos botões e tamanho do cursor do rato -b | configurar a ordem dos botões para dextros/canhotos -h | exibir este diálogo de ajuda -s | configurar o tamanho do cursor Ajuda: Sem configuração de início de interface valor fora do permitido para aceleração ou limite, assim, aplicam-se os valores xset por defeito  