��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  @   �  U     N   j     �  "   �     �  U     ]   Y           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2021
Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 -a   | definir o movimento do rato/mouse (aceleração e limite) -all | definir o movimento do rato/mouse, a ordem dos botões e o tamanho \ndo cursor -b   | definir a ordem dos botões para a mão esquerda ou para a mão direita -h   | exibe esta tela de ajuda -s   | definir o tamanho do cursor Ajuda: Nenhuma opção de configuração para iniciar a Interface Gráfica do Usuário - GUI o valor está fora da faixa limite para a aceleração, aplicando os valores padrão\ndo xset 