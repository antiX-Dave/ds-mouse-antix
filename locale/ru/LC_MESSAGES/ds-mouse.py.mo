��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �    &  �   +  �   �  �   E  P   �  G     U   _  �   �  �   x           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Robin, 2021
Language-Team: Russian (https://www.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
   -a   | Определение реакции указателя мыши.
         (Коэффициент ускорения и порог)   -all | Настройка ускорения мыши, назначения,
         клавиш и размера указателя.   -b   | Настройка назначения кнопок для
         мыши для правшей или левшей   -h   | Отображение этой справки по программе.   -s   | Установка размера указателя мыши. Помощь для программы:
  Опции командной строки:   Если ни один из параметров командной строки не указан,
  запускается графический интерфейс пользователя. Значение ускорения или порога срабатывания находится вне допустимого диапазона. Поэтому он заменяется значениями по умолчанию "xset". 