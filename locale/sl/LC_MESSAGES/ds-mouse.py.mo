��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  0   �  B   (  <   k  $   �      �     �  2   �  d   )           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2021
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 -a   | določi premik miške (pospešek in prag) -all | določi premik miške, razpored gumbov in velikost kurzorja -b   | določi razpored gumbov za levičarje ali desničarje -h   | prikaz tega dialog s pomočjo -s   | določi velikost kurzorja Pomoč: Brez opcije zažene nastavitveni grafični vmesnik Vrednost izven obsega (out-of-range) za pospešek ali prag. Uporabljene bodo privzete xset vrednosti 