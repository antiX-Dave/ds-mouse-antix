��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  3   �  @   �  E   9  !        �     �  0   �  g   �           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2021
Language-Team: Albanian (https://www.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 -a   | udjisni lëvizje miu (përshpejtim dhe prag) -all | ujdisni lëvizje miu, rend butonash, dhe madhësi kursori -b   | caktoni radhë butonash për dorë të majtë dhe të djathtë -h   | shfaq këtë dialog ndihme -s   | caktoni madhësi kursori Ndihmë: Pa u zgjedhur mundësi, hapet GUI i rregullimeve vlerë jashtë intervali, për përshpejtim ose prag, ndaj po aplikohen vlerat parazgjedhje të xset-it 