��    	      d      �       �   4   �   6     /   M     }     �     �     �  Q   �  �  &  8   �  B   �  <   <     y  $   �     �  )   �  ^   �           	                                   -a   | set mouse motion (acceleration and threshold) -all | set mouse motion, button order, and cursor size -b   | set button order for left and right hand -h   | show this help dialog -s   | set cursor size Help: No option start settings gui out-of-range value for acceleration or threshold, so applying xset default values Project-Id-Version: ds-mouse version (...)
Report-Msgid-Bugs-To: antixforum.com
PO-Revision-Date: 2021-09-18 09:49+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2021
Language-Team: Swedish (https://www.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 -a   | ställ in musrörelse (acceleration och tröskel) -all | ställ in musrörelse, knappordning och muspekarens storlek -b   | ställ in knappordning för vänster och höger  hand -h   | visa denna hjälpdialog -s   | ställ in muspekarens storlek Hjälp: Ingen möjlighet starta inställnings gui Accelerations eller tröskelvärde utanför området, tillämpar därför xset standardvärden 